package com.abot.mytodolist.Utils;

import android.graphics.Paint;
import android.widget.TextView;

/**
 * Created by YingLi on 8/30/17.
 */

public class UIUtils {

    public static void setTextViewStrikeThrough(TextView tv, boolean strikeThrough){
        if(strikeThrough){
            tv.setPaintFlags(tv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }else{
            tv.setPaintFlags(tv.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
        }
    }
}
