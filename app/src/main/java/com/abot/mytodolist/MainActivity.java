package com.abot.mytodolist;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.abot.mytodolist.Models.Todo;
import com.abot.mytodolist.Utils.DateUtils;
import com.abot.mytodolist.Utils.ModelUtils;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final int REQ_CODE_TODO_EDIT = 100;

    private static final String TODOS = "todos";

    private List<Todo> todos = new ArrayList<Todo>();
    private MyListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, TodoEditActivity.class);
                startActivityForResult(intent, REQ_CODE_TODO_EDIT);
            }
        });

        loadData();
        adapter = new MyListAdapter(this, todos);
        ListView listView = findViewById(R.id.main_list_view);
        listView.setAdapter(adapter);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQ_CODE_TODO_EDIT && resultCode == Activity.RESULT_OK){
            String todoId = data.getStringExtra(TodoEditActivity.KEY_TODO_ID);
            if(todoId != null){
                deleteTodo(todoId);
            }else{
                Todo todo = data.getParcelableExtra(TodoEditActivity.KEY_TODO);
                updateTodo(todo);
            }
        }
    }


    private void updateTodo(Todo todo){
        boolean found = false;
        for(int i=0; i<todos.size(); i++){
            Todo item = todos.get(i);
            if(TextUtils.equals(item.id, todo.id)){
                found = true;
                todos.set(i, todo);
                break;
            }
        }
        if(found == false){
            todos.add(todo);
        }

        adapter.notifyDataSetChanged();
        ModelUtils.save(this, TODOS, todos);
    }

    public void updateTodo(int position, boolean done){
        todos.get(position).done = done;

        adapter.notifyDataSetChanged();
        ModelUtils.save(this, TODOS, todos);
    }

    private void deleteTodo(@NonNull String todoId){
        for(int i=0; i<todos.size(); i++){
            Todo item = todos.get(i);
            if(TextUtils.equals(item.id, todoId)){
                todos.remove(i);
                break;
            }
        }

        adapter.notifyDataSetChanged();
        ModelUtils.save(this, TODOS, todos);
    }

    private void loadData(){
        todos = ModelUtils.read(this, TODOS, new TypeToken<List<Todo>>(){});
        if(todos == null){
            todos = new ArrayList<>();
        }
    }
}
