package com.abot.mytodolist;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.abot.mytodolist.Models.Todo;
import com.abot.mytodolist.Utils.UIUtils;

import java.util.List;

/**
 * Created by YingLi on 8/24/17.
 */

public class MyListAdapter extends BaseAdapter {

    private MainActivity activity;
    private List<Todo> data;

    public MyListAdapter(MainActivity activity, List<Todo> data){
        this.activity = activity;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        ViewHolder vh;
        if(view == null){
            vh = new ViewHolder();
            view = LayoutInflater.from(activity).inflate(R.layout.main_item_list, viewGroup, false);
            vh.todoText = view.findViewById(R.id.main_list_item_text);
            vh.doneCheckbox = view.findViewById(R.id.main_list_item_check);
            view.setTag(vh);
        }else{
            vh = (ViewHolder) view.getTag();
        }
        final Todo todo = (Todo)getItem(position);
        vh.todoText.setText(todo.text);
        vh.doneCheckbox.setChecked(todo.done);
        UIUtils.setTextViewStrikeThrough(vh.todoText, todo.done);

        vh.doneCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                activity.updateTodo(position, isChecked);
            }
        });

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, TodoEditActivity.class);
                intent.putExtra(TodoEditActivity.KEY_TODO, todo);
                activity.startActivityForResult(intent, activity.REQ_CODE_TODO_EDIT);
            }
        });

        return view;
    }

    private static class ViewHolder{
        TextView todoText;
        CheckBox doneCheckbox;
    }
}
